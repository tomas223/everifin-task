# Everifin task

## Installation & Start

```bash
# install deps
yarn

# start dev server
node ace serve --watch
```

## Endpoints

```bash
# Create random numbers
curl -v -X POST 0.0.0.0:3333/numbers -H 'Content-Type: application/json' -d '{"quantity":10,"randomMax":15}'

# Return Nth number from previous create request
curl -v 0.0.0.0:3333/numbers/nth/1

# Return previous data
curl -v 0.0.0.0:3333/numbers
```