import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateNumberValidator from 'App/Validators/CreateNumberValidator'
import Database from 'App/Services/Database'

export default class NumbersController {
  private db = new Database()

  public async index(ctx: HttpContextContract) {
    const result = await this.db.readLatestRecord()
    const resultNums = result.map((val) => Number(val))

    const max = Math.max(...resultNums)
    const min = Math.min(...resultNums)
    const sum = resultNums.reduce((acc, curr) => acc + curr, 0)

    return { result, max, min, sum }
  }

  public async store(ctx: HttpContextContract) {
    const { quantity, randomMax } = await ctx.request.validate(CreateNumberValidator)

    const numbers = Array.from({ length: quantity }, () => Math.round(Math.random() * randomMax))
    await this.db.storeBatch(numbers)

    return numbers
  }

  public async getNth(ctx: HttpContextContract) {
    const { nth } = ctx.request.params()
    if (nth === 0) return { result: null }

    const { id, size } = await this.db.findLatestRecord()

    if (nth > size)
      return ctx.response
        .status(422)
        .send({ error: "Requested number doesn't exist. Please provide number 0 <= x <= N" })

    const valueArray = await this.db.readRecord(id)

    return { result: valueArray[nth - 1] }
  }
}
