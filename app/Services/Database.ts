import { storageDbPath } from 'Config/app'
import fs from 'fs'
import Path from 'path'
import shortUUID from 'short-uuid'
const indexPath = Path.join(storageDbPath, 'index.data')

export const dbInit = () => {
  console.log('DatabaseService init')
  fs.promises.mkdir(storageDbPath, { recursive: true })
  fs.promises.writeFile(indexPath, '', { flag: 'a+' })
}

type RecordMeta = {
  id: string
  size: number
}

export default class DatabaseService {
  protected rootPath: string = storageDbPath

  private uuid() {
    const id = `${new Date().valueOf()}-${shortUUID.generate()}`
    return id
  }

  public async storeBatch(data: Array<number | string>) {
    const id = this.uuid()
    const dir = `${Path.join(this.rootPath, id)}${Path.sep}`
    await fs.promises.mkdir(dir)

    const settled = await Promise.allSettled(
      data.map(async (val, index) => {
        await fs.promises.writeFile(`${dir}${index}`, val.toString())
      })
    )

    if (settled.find((writeSuccess) => !writeSuccess)) {
      throw new Error('Error saving all data')
    }

    const indexRecord = `${id};${data.length}\n`
    await fs.promises.writeFile(indexPath, indexRecord, { flag: 'a+' })
  }

  public async findLatestRecord(): Promise<RecordMeta> {
    const dbIndex = await fs.promises.readFile(indexPath)
    const latestRecord = dbIndex.toString().split('\n').sort().slice(-1)[0]
    const [id, size] = latestRecord.split(';')

    return { id, size: Number(size) }
  }

  public async readRecord(id: string): Promise<string[]> {
    const dirPath = `${Path.join(this.rootPath, id)}${Path.sep}`
    const fileList = await fs.promises.readdir(dirPath)

    const readingPromises = fileList.map(async (fileName) => {
      const path = `${dirPath}${fileName}`
      return (await fs.promises.readFile(path)).toString()
    })

    const results = (await Promise.allSettled(readingPromises)).filter(
      (result) => result.status === 'fulfilled'
    ) as PromiseFulfilledResult<string>[]
    const dataArray = results.map((res) => res.value)
    return dataArray
  }

  public async readLatestRecord(): Promise<string[]> {
    const { id } = await this.findLatestRecord()
    return await this.readRecord(id)
  }
}
