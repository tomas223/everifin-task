/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('numbers', 'NumbersController.index')

Route.get('numbers/nth/:nth', 'NumbersController.getNth').where('nth', {
  match: /^[0-9]+$/,
  cast: (id) => Number(id),
})

Route.post('numbers', 'NumbersController.store')
